//our-domain/news
import Link from "next/link";
import { Fragment } from "react";

function NewsPage() {
  return (
    <Fragment>
      <h1>News Page</h1>
      <ul>
        <li>
          <Link href="/news/first-news-item">First news item</Link>
        </li>
        <li>
          <Link href="/news/second-news-item">Second news item</Link>
        </li>
      </ul>
    </Fragment>
  );
}

export default NewsPage;
